var monographs;
var jarticles;
var indexOfUpdatingMonograph;
var indexOfUpdatingArticle;

function readMyMonograph() {

  const Http = new XMLHttpRequest();
  const url='http://localhost:8080/monographs';
  Http.open("GET", url);
  Http.send();
  Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)
	var obj = JSON.parse(Http.responseText);
	monographs = obj;
	document.getElementById("show_mono").innerHTML = "";
	for(i = 0; i < obj.length; i++){
		
    document.getElementById("show_mono").innerHTML += "<table><tr><td style=\"width: 75%\">" + obj[i].author.lastName + " " + obj[i].author.firstName + ", " + "<i>" + obj[i].title + "</i>" + ", " + "Wyd. " + obj[i].publishingHouse + ", " + obj[i].city + " " + obj[i].year + "</td><td>" +
	"<button id=\"delete_monograph\" onclick=\"deleteMonograph(" + obj[i].monographId + ")\"><img src=\"images/rubbish-bin.png\" height='32'></button>" +
	"<button id=\"edit_monograph\" onclick=\"editMonograph(" + i + ")\"><img src=\"images/edit.png\" height='32'></button>" + "</td></tr></table" +
	"<br>";
	}
	}
  }
}

function readMyJournalArticle(){
	const Http = new XMLHttpRequest();
  const url='http://localhost:8080/journalArticles';
  Http.open("GET", url);
  Http.send();
  Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)
	var obj = JSON.parse(Http.responseText);
	jarticles = obj;
	document.getElementById("show_journal_article").innerHTML = "";
	for(i = 0; i < obj.length; i++){
		
    document.getElementById("show_journal_article").innerHTML += "<table><tr><td style=\"width: 75%\">" + obj[i].author.lastName + " " + obj[i].author.firstName + ", " + "<i>" + obj[i].title + "</i>" + ", " + "\"" + obj[i].journal.title + "\"" + ", " + "nr " + obj[i].journal.issue + ", " + obj[i].journal.year + "</td><td>" +
	"<button id=\"delete_journal_article\" onclick=\"deleteJournalArticle(" + obj[i].articleId + ")\"><img src=\"images/rubbish-bin.png\" height='32'></button>" + 
	"<button id=\"edit_journal_article\" onclick=\"editJournalArticle(" + i + ")\"><img src=\"images/edit.png\" height='32'></button>" + "</td></tr></table" +
	"<br>";
	}
	}
  }
}

function addMonograph(){
	document.getElementById("add_my_monograph").style.display = "block";
}


function addJournalArticle(){
	document.getElementById("add_my_journal_article").style.display = "block";
}


function addAuthor(fn, ln){
	
	var author = new Object();
	author.firstName = document.getElementsByName(fn)[0].value;
	author.lastName = document.getElementsByName(ln)[0].value;
	var authorJson = JSON.stringify(author);
	console.log(authorJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/authors';
	Http.open("POST", url);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(authorJson);
}

function selectAuthor(c){
	var x = document.getElementById(c);

	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/authors';
	Http.open("GET", url);
	Http.send();
	Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)
	var obj = JSON.parse(Http.responseText);
	
	for(i = 0; i < obj.length; i++){
		var option = document.createElement("option");
		option.text += obj[i].lastName + " " + obj[i].firstName;	
		option.value = JSON.stringify(obj[i]);
		x.add(option);
   
	}
	}
  }
}



function submitMonograph(){
	var monograph = new Object();
	monograph.author = document.getElementById("authors").value;
	monograph.title = document.getElementsByName("mono_title")[0].value;
	monograph.publishingHouse = document.getElementsByName("mono_publishing")[0].value;
	monograph.city = document.getElementsByName("mono_city")[0].value;
	monograph.year = document.getElementsByName("mono_year")[0].value;
	var monographJson = JSON.stringify(monograph);
	console.log(monographJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/monographs';
	Http.open("POST", url);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(monographJson);
// po naciśnięciu przycisku dodania pozycji, formularze znikają
	
	document.getElementById("add_my_monograph").style.display = "none";
	alert('Udało Ci się dodać książkę!:) Aby zobaczyć, ponownie wczytaj książki:)');
}

function submitJournalArticle(){
	var journalArticle = new Object();
	journalArticle.journal = new Object();
	
	journalArticle.author = document.getElementById("jAuthors").value;
	journalArticle.journal = document.getElementById("journal_combobox").value;
	
	journalArticle.title = document.getElementsByName("journal_article_title")[0].value;
	var journalArticleJson = JSON.stringify(journalArticle);
	console.log(journalArticleJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/journalArticles';
	Http.open("POST", url);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(journalArticleJson);
	
	document.getElementById("add_my_journal_article").style.display = "none";
	alert('Udało Ci się dodać artykuł!:)');
}

function deleteMonograph(monographId)
{
 const Http = new XMLHttpRequest();
  const url='http://localhost:8080/monographs/'+monographId;
  Http.open("DELETE", url);
  Http.send();
  Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)	
	}
	}	
	alert('Udało Ci się usunąć książkę!:)');
}

function deleteJournalArticle(articleId)
{
 const Http = new XMLHttpRequest();
  const url='http://localhost:8080/journalArticles/'+articleId;
  Http.open("DELETE", url);
  Http.send();
  Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)	
	}
	}
	alert('Udało Ci się usunąć artykuł!:)');
}
  
  
function addJournal(jtitle, jissue, jyear){
	
	var journal = new Object();
	journal.title = document.getElementsByName(jtitle)[0].value;
	journal.issue = document.getElementsByName(jissue)[0].value;
	journal.year = document.getElementsByName(jyear)[0].value;
	var journalJson = JSON.stringify(journal);
	console.log(journalJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/journals';
	Http.open("POST", url);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(journalJson);
}


function selectJournal(sj){
	var x = document.getElementById(sj);

	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/journals';
	Http.open("GET", url);
	Http.send();
	Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)
	var obj = JSON.parse(Http.responseText);
	
	for(i = 0; i < obj.length; i++){
		var option = document.createElement("option");
		option.text += obj[i].title + ", " + "nr " + obj[i].issue + ", " + obj[i].year;	
		option.value = JSON.stringify(obj[i]);
		x.add(option); 
	}
	}
  }	
}

function editMonograph(index){
// uzupełnianie pól formularza po kliknięciu na button 'edit'
	document.getElementById("edit_my_monograph").style.display = "block";
	indexOfUpdatingMonograph = index;
	document.getElementsByName("update_f_name")[0].value = monographs[index].author.firstName;
	document.getElementsByName("update_l_name")[0].value = monographs[index].author.lastName;	
	document.getElementsByName("update_mono_title")[0].value = monographs[index].title;
	document.getElementsByName("update_mono_publishing")[0].value = monographs[index].publishingHouse;
	document.getElementsByName("update_mono_city")[0].value = monographs[index].city;
	document.getElementsByName("update_mono_year")[0].value = monographs[index].year;
	}

function editJournalArticle(index){
	document.getElementById("edit_my_journal_article").style.display = "block";
	indexOfUpdatingArticle = index;
	document.getElementsByName("update_a_f_name")[0].value = jarticles[index].author.firstName;
	document.getElementsByName("update_a_l_name")[0].value = jarticles[index].author.lastName;
	document.getElementsByName("update_article_title")[0].value = jarticles[index].title;
	document.getElementsByName("update_journal_title")[0].value = jarticles[index].journal.title;	
	document.getElementsByName("update_issue")[0].value = jarticles[index].journal.issue;
	document.getElementsByName("update_article_year")[0].value = jarticles[index].journal.year;
}	
	
function updateMonograph()
{
	
	var monograph = new Object();
	monograph.author = new Object();
	monograph.monographId = monographs[indexOfUpdatingMonograph].monographId;
	
	monograph.author = document.getElementById("update_combobox_authors").value;
	monograph.title = document.getElementsByName("update_mono_title")[0].value;
	monograph.publishingHouse = document.getElementsByName("update_mono_publishing")[0].value;
	monograph.city = document.getElementsByName("update_mono_city")[0].value;
	monograph.year = document.getElementsByName("update_mono_year")[0].value;
	
	var monographJson = JSON.stringify(monograph);
	console.log(monographJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/monographs';
	Http.open("PUT", url);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(monographJson);
	
	alert('Udało Ci się edytować książkę!:)');
}

function updateJournalArticle()
{
	
	var journalArticle = new Object();
	journalArticle.journal = new Object();
	journalArticle.articleId = jarticles[indexOfUpdatingArticle].articleId;
	
	journalArticle.author = document.getElementById("update_j_authors_combobox").value;
	journalArticle.journal = document.getElementById("update_journal_combobox").value;
	
	journalArticle.title = document.getElementsByName("update_article_title")[0].value;

	var journalArticleJson = JSON.stringify(journalArticle);
	console.log(journalArticleJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/journalArticles';
	Http.open("PUT", url);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(journalArticleJson);
	
	alert('Udało Ci się edytować artykuł!:)');
}


